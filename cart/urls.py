from django.urls import path

from cart import views


urlpatterns = [
    path("list/",views.List.as_view()),
    path("products/",views.ProductList.as_view()),
    path("products/<int:pk>",views.ProductDetails.as_view()),
    path("order/",views.OrderDetails.as_view()),
    path("order/<int:pk>",views.OrderUpdate.as_view()),
    path("orderitem/",views.OrderItemDetails.as_view()),
    path("orderitem/<int:pk>",views.OrderItemUpdate.as_view()),
]