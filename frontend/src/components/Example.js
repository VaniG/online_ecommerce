import axios from 'axios';

import React, { Component } from 'react';



class Home extends React.Component {

    state = {
        details: [],
    }

    componentDidMount() {

        let data;

        axios.get('http://127.0.0.1:8000/api/products/')
            .then(res => {
                data = res.data;
                this.setState({
                    details: data
                });
            })
            .catch(err => {
            })
    }

    render() {

        return (

            <div id="product">

                {this.state.details.map((detail, id) => (

                             <div key={detail.id}>

                            <div>
                                <div>
                                    <h1>{detail.Title} </h1>
                                <img src= {detail.ImageLink} alt='img' />
                                         <span>Rs:{detail.Price}</span>
                               <p>{detail.Description}</p>
                                    <button onClick={(detail.id)}>Add to cart</button>


                                </div>
                            </div>
                        </div>
                    )
                )}
            </div>
        );
    }
}

export default Home;